/******************
 * Author: Tungpsse141173
 * Created Date: 26/11/2019
 * Updated Date: 26/11/2019
 ******************/

package fpt.edu.vn.swd301_appmobile.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import fpt.edu.vn.swd301_appmobile.R;
import fpt.edu.vn.swd301_appmobile.home.HomeActivity;
import fpt.edu.vn.swd301_appmobile.testactivity.TestActivity;

public class LoginActivity_MainScreen extends AppCompatActivity {
    //Google Login Request Code
    private int RC_SIGN_IN = 7;
    //Google Sign In Client
    private static GoogleSignInClient mGoogleSignInClient = null;
    //Firebase Auth
    private static FirebaseAuth mAuth = null;

    //logInPresenter
    private static LogInPresenter logInPresenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__main_screen);
        //gg option cai nay neu nam ngoai onCreate se loi
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        ImageView login = findViewById(R.id.img_Google);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                handleSignIn();
            }
        });
        logInPresenter = new LogInPresenter();

        //Move toi trang home
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity_MainScreen.this, HomeActivity.class);
                startActivity(intent);
            }
        });


    }

    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.txtContact:
                makePhoneCall();

                break;


        }
        if (null != intent) startActivity(intent);

    }

    // Khi nhan vao contact se call
    public void makePhoneCall() {
        Intent intent = new Intent(Intent.ACTION_CALL);

        if (ActivityCompat.checkSelfPermission(LoginActivity_MainScreen.this,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            intent.setData(Uri.parse("tel:12345678"));
            Toast.makeText(this, "call success", Toast.LENGTH_LONG).show();

        } else {


            Toast.makeText(this, "fail", Toast.LENGTH_LONG).show();
            return;

        }
        startActivity(intent);
    }

    public void handleSignIn() {


        Intent signInIntent = mGoogleSignInClient.getSignInIntent();


        //ko the la func cua presenter
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("CHECK1", "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("CHECK4", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("CHECK2", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();


                            //goi presenter
                            logInPresenter.handleSignIn(user);
                            //muon chuyen sang home
                            Intent intent = new Intent(
                                    LoginActivity_MainScreen.this, TestActivity.class);
                            intent.putExtra("username", user.getDisplayName());
                            startActivity(intent);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("CHECK3", "signInWithCredential:failure",
                                    task.getException());
                            Snackbar.make(findViewById(R.id.login_layout),
                                    "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            logInPresenter.handleSignIn(null);

                        }

                        // ...
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.

        if (mAuth != null) {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser != null) {

                //goi presenter
                logInPresenter.handleSignIn(currentUser);
                Log.d("CHECK7", "onStart: " + currentUser);
                //muon chuyen sang home
                Intent intent = new Intent(LoginActivity_MainScreen.this, TestActivity.class);
                intent.putExtra("username", currentUser.getDisplayName());

                startActivity(intent);
                finish();
            } else {

            }

        } else {

        }

    }

}


