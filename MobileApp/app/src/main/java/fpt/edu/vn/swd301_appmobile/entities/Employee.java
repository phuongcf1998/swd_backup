package fpt.edu.vn.swd301_appmobile.entities;

import java.sql.Date;

public class Employee {
    //employee id

    private String id;
    //company id

    private String companyid;
    //first name

    private String firstname;
    //last name

    private String lastname;
    //gender

    private int gender;
    //date of birth

    private Date dob;
    //phone number

    private int phoneNumber;
    //email

    private String email;
    //address

    private String address;
    //country

    private String country;
    //username

    private String username;
    //password

    private String password;
    //token key

    private String tokenKey;
    //level

    private int level;

    public Employee() {
    }

    public Employee(String id, String companyid, String firstname, String lastname, int gender, Date dob, int phoneNumber, String email, String address, String country, String username, String password, String tokenKey, int level) {
        this.id = id;
        this.companyid = companyid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.dob = dob;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.country = country;
        this.username = username;
        this.password = password;
        this.tokenKey = tokenKey;
        this.level = level;
    }

    public Employee(String username, int level) {
        this.username = username;
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
