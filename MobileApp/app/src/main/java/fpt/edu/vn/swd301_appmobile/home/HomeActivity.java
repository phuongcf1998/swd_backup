package fpt.edu.vn.swd301_appmobile.home;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import fpt.edu.vn.swd301_appmobile.R;
import fpt.edu.vn.swd301_appmobile.wallet.CashWalletActivity;
import fpt.edu.vn.swd301_appmobile.wallet.FnBWalletActivity;
import fpt.edu.vn.swd301_appmobile.wallet.HotWalletActivity;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);



    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardview_cash:
                Intent intent = new Intent (HomeActivity.this, CashWalletActivity.class);
                startActivity(intent);


                break;
            case R.id.cardview_fnb:
                Intent intent1 = new Intent (HomeActivity.this, FnBWalletActivity.class);
                startActivity(intent1);
                break;

            case R.id.cardview_hot:
                Intent intent2 = new Intent (HomeActivity.this, HotWalletActivity.class);
                startActivity(intent2);
                break;




        }


    }
}
