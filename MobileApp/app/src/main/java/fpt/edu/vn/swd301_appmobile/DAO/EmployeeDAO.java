package fpt.edu.vn.swd301_appmobile.DAO;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import fpt.edu.vn.swd301_appmobile.entities.Employee;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.List;
public class EmployeeDAO {
    private static final String BASE_URL = "http://localhost:8080/employees";
    public ArrayList<Employee> readAll()  {
        InputStream is = null;
        String respone = null;
        HttpResponse httpResponse;
        try{

            HttpClient  httpClient = new DefaultHttpClient();

            HttpGet httpGet = new HttpGet(BASE_URL);

            httpResponse = httpClient.execute(httpGet);

            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {

                // A Simple JSON Response Read
                is = httpEntity.getContent();

                // now you have the string representation of the HTML request
                System.out.println("RESPONSE: " + is);


            }

//            is = myConnection.getInputStream();

        }catch (Exception e){
            Log.e("Buffer error", "create: "+e.toString() );
        }
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"UTF-8"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = bufferedReader.readLine()) != null){
                sb.append(line +" \n");
                Log.d("LINE", "readAll: " + line);
            }
            is.close();
            respone = sb.toString();
        }catch (Exception e){
            Log.e("Buffer error", "create: "+e.toString() );
        }
        try{
            JSONArray jsonArray = new JSONArray(respone);
            ArrayList<Employee> listEmployee = new ArrayList<>();
            for (int i = 0; i<jsonArray.length();i++){
                JSONObject item = jsonArray.getJSONObject(i);
                Employee employee = new Employee(item.getString("username"),item.getInt("level"));


                listEmployee.add(employee);
            }
            Log.d("JsonData", "readAll: " + listEmployee);
            return listEmployee;
        }catch (Exception e){
            Log.e("Buffer error", "create: "+e.toString() );
            return new ArrayList<Employee>();
        }
    }
}
