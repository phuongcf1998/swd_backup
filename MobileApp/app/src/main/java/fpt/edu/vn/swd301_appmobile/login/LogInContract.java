/******
 *Author: Tungpsse141173
 * Created Date: 27/11/2019
 * Updated Date: 27/11/2019
 */

package fpt.edu.vn.swd301_appmobile.login;

import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public interface LogInContract {
    interface View{
        void signInSuccess();

        void signInFailure(String errorMsg);
    }

    interface Presenter{
        void handleSignIn(FirebaseUser user);
    }
}
