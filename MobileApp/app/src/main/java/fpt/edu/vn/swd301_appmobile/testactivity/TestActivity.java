package fpt.edu.vn.swd301_appmobile.testactivity;
/******************
 * Author: PhuNNSE62953
 * Created Date: 28/11/2019
 * Updated Date: 28/11/2019
 ******************/
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import fpt.edu.vn.swd301_appmobile.DAO.EmployeeDAO;
import fpt.edu.vn.swd301_appmobile.R;
import fpt.edu.vn.swd301_appmobile.entities.Employee;
import fpt.edu.vn.swd301_appmobile.login.LoginActivity_MainScreen;
//cai nay de test gg login voi logout
public class TestActivity extends AppCompatActivity {
    //Firebase Auth cai nay call o moi activity
    private static FirebaseAuth mAuth = null;
    //Google Sign In Client
    private static GoogleSignInClient mGoogleSignInClient = null;
    List<Employee> employeeList = null;
    Employee employee = null;
    EmployeeDAO employeeDAO = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Intent intent = this.getIntent();
        intent.getStringExtra("username");
        TextView welcome = findViewById(R.id.welcome);
        welcome.setText(intent.getStringExtra("username"));
        Button logout = findViewById(R.id.logout);
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                mGoogleSignInClient.signOut().addOnCompleteListener(TestActivity.this, task -> {
                    Intent intent = new Intent(TestActivity.this, LoginActivity_MainScreen.class);

                    startActivity(intent);
                    finish();
                });

            }
        });
        employeeDAO = new EmployeeDAO();
        employeeList = employeeDAO.readAll();
        LinearLayout linearLayout = findViewById(R.id.temporary);

        if(employeeList.size() <= 0){
            TextView textView = new TextView(this);
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText("No data");
            linearLayout.addView(textView);
        }else{
            for (int i = 0; i < employeeList.size(); i++ ){
                TextView textView = new TextView(this);
                textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                textView.setText(employeeList.get(i).toString());
                linearLayout.addView(textView);
            }
        }
    }

}
