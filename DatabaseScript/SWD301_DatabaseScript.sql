USE [master]
GO
/****** Object:  Database [SWD301]    Script Date: 11/26/2019 6:28:08 PM ******/
CREATE DATABASE [SWD301]

GO
ALTER DATABASE [SWD301] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SWD301].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SWD301] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SWD301] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SWD301] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SWD301] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SWD301] SET ARITHABORT OFF 
GO
ALTER DATABASE [SWD301] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SWD301] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SWD301] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SWD301] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SWD301] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SWD301] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SWD301] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SWD301] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SWD301] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SWD301] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SWD301] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SWD301] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SWD301] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SWD301] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SWD301] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SWD301] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SWD301] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SWD301] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SWD301] SET  MULTI_USER 
GO
ALTER DATABASE [SWD301] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SWD301] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SWD301] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SWD301] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SWD301] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SWD301]
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyID] [nchar](400) NOT NULL,
	[CompanyName] [nchar](50) NOT NULL,
	[DirectorID] [nchar](500) NOT NULL,
	[JoinedDate] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Emp1PaySlips]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emp1PaySlips](
	[EmployeeID] [nchar](400) NOT NULL,
	[PayrollPeriodContentID] [nchar](400) NOT NULL,
	[PaycheckLevel] [smallint] NOT NULL,
	[P1] [bigint] NOT NULL,
	[P2] [bigint] NOT NULL,
	[P3] [bigint] NOT NULL,
	[TotalPoint] [bigint] NOT NULL,
	[UpLevelBonus] [bigint] NOT NULL,
	[OtherRewards] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeID] [nchar](400) NOT NULL,
	[CompanyID] [nchar](400) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Gender] [bit] NOT NULL,
	[DOB] [date] NOT NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Email] [nchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Country] [nchar](50) NOT NULL,
	[UserName] [nchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[TokenKey] [nvarchar](max) NULL,
	[Level] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaycheckLevels]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaycheckLevels](
	[PaycheckLevel] [smallint] NOT NULL,
	[AmountPerHour] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PaycheckLevel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayrollPeriodContents]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollPeriodContents](
	[ContentID] [nchar](400) NOT NULL,
	[PeriodID] [nchar](400) NOT NULL,
	[EmployeeID] [nchar](400) NOT NULL,
	[TotalPoint] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayrollPeriods]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayrollPeriods](
	[PeriodID] [nchar](400) NOT NULL,
	[CompanyID] [nchar](400) NOT NULL,
	[Date] [date] NOT NULL,
	[Time] [time](7) NOT NULL,
	[Status] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[TransactionID] [nchar](400) NOT NULL,
	[FromWalletID] [nchar](400) NOT NULL,
	[ToWalletID] [nchar](400) NOT NULL,
	[Date] [date] NOT NULL,
	[Time] [time](7) NOT NULL,
	[Amount] [bigint] NOT NULL,
	[Status] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Wallets]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallets](
	[WalletID] [nchar](400) NOT NULL,
	[EmployeeID] [nchar](400) NOT NULL,
	[WalletTypeID] [smallint] NOT NULL,
	[Amount] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[WalletID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WalletTypes]    Script Date: 11/26/2019 6:28:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WalletTypes](
	[WalletTypeID] [smallint] NOT NULL,
	[WalletTypeName] [nchar](10) NOT NULL,
	[MeasurementUnit] [nchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[WalletTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Emp1PaySlips]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[Emp1PaySlips]  WITH CHECK ADD FOREIGN KEY([PaycheckLevel])
REFERENCES [dbo].[PaycheckLevels] ([PaycheckLevel])
GO
ALTER TABLE [dbo].[Emp1PaySlips]  WITH CHECK ADD FOREIGN KEY([PayrollPeriodContentID])
REFERENCES [dbo].[PayrollPeriodContents] ([ContentID])
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Companies] ([CompanyID])
GO
ALTER TABLE [dbo].[PayrollPeriodContents]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[PayrollPeriodContents]  WITH CHECK ADD FOREIGN KEY([PeriodID])
REFERENCES [dbo].[PayrollPeriods] ([PeriodID])
GO
ALTER TABLE [dbo].[PayrollPeriods]  WITH CHECK ADD FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Companies] ([CompanyID])
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD FOREIGN KEY([FromWalletID])
REFERENCES [dbo].[Wallets] ([WalletID])
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD FOREIGN KEY([ToWalletID])
REFERENCES [dbo].[Wallets] ([WalletID])
GO
ALTER TABLE [dbo].[Wallets]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[Wallets]  WITH CHECK ADD FOREIGN KEY([WalletTypeID])
REFERENCES [dbo].[WalletTypes] ([WalletTypeID])
GO
USE [master]
GO
ALTER DATABASE [SWD301] SET  READ_WRITE 
GO
