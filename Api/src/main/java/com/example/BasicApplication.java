package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@EnableSwagger2
@SpringBootApplication
@ComponentScan("com.example")
public class BasicApplication {

    
    
   
    public static void main(String[] args) {
        SpringApplication.run(BasicApplication.class, args);
    }
}
