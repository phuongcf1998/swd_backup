/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.PayslipEachEmployee;
import com.example.services.PayslipEachEmployeeService;
import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/payslipseachemployees")

public class PayslipEachEmployeeController {

    @Autowired
    private PayslipEachEmployeeService service;
    //get all Payslip
    @GetMapping("/readAll")
    Iterable<PayslipEachEmployee> readAll() {
        return service.findAll();
    }
    //get payslip by id

    @GetMapping("/read/{id}")
    PayslipEachEmployee read(@PathVariable String id) {
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("/create")
    //create payslip
    PayslipEachEmployee create(@RequestBody PayslipEachEmployee newPayslipEachEmployee) {
        //out of scope
        return service.save(newPayslipEachEmployee);
    }

    @PutMapping("/update/{id}")
    //update payslip
    PayslipEachEmployee update(@RequestBody PayslipEachEmployee editedPayslipEachEmployee, @PathVariable String id) {
        return service.findById(id)
                .map(PayslipEachEmployee -> {
                    //out of scope
                    
                    return service.save(PayslipEachEmployee);
                })
                .orElseGet(() -> {
                    editedPayslipEachEmployee.setPayrollperiodcontentid(id);
                    return service.save(editedPayslipEachEmployee);
                });
    }
    //remove payslip
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
