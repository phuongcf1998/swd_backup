/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.Employee;
import com.example.services.EmployeeService;
import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/employees")
@Api(value = "onlinemanagement", description = "Employee management")
public class EmployeeController {

    @Autowired
    private EmployeeService service;
    //get all employees
    @GetMapping("")
    Iterable<Employee> readAll() {
        return service.findAll();
    }
    //get all employees
    @GetMapping("/{companyid}")
    Iterable<Employee> readByCompany() {
        return service.findAll();
    }
    //get employee by id

    @GetMapping("/{id}")
    Employee read(@PathVariable String id) {
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("")
    //create an employee
    Employee create(@RequestBody Employee newEmployee) {
        
        return service.save(newEmployee);
    }

    @PutMapping("/{id}")
    //update employee level
    Employee update(@RequestBody Employee editedEmployee, @PathVariable String id) {
        return service.findById(id)
                .map(Employee -> {
                    
                    
                    return service.save(Employee);
                })
                .orElseGet(() -> {
                    editedEmployee.setId(id);
                    return service.save(editedEmployee);
                });
    }
    //remove an employee
    @DeleteMapping("/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
