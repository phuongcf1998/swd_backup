/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.PaycheckLevels;
import com.example.services.PaycheckLevel;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/paychecklevels")

public class PaycheckLevelController {

    @Autowired
    private PaycheckLevel service;
    //get all PaycheckLevel
    @GetMapping("")
    Iterable<PaycheckLevels> readAll() {
        //out of scope
        return service.findAll();
    }
    
    //get PaycheckLevel by id

    @GetMapping("/{id}")
    PaycheckLevels read(@PathVariable String id) {
        //out of scope
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("")
    //create PaycheckLevel
    PaycheckLevels create(@RequestBody PaycheckLevels newPaycheckLevels) {
        //out of scope
        return service.save(newPaycheckLevels);
    }

    @PutMapping("/{id}")
    //update PaycheckLevel
    PaycheckLevels update(@RequestBody PaycheckLevels editedPaycheckLevels, @PathVariable String id) {
        //out of scope
        return service.findById(id)
                .map(PaycheckLevels -> {
                    
                    
                    return service.save(PaycheckLevels);
                })
                .orElseGet(() -> {
                    editedPaycheckLevels.setPaychecklevel(Integer.parseInt(id));
                    return service.save(editedPaycheckLevels);
                });
    }
    //remove PaycheckLevel
    @DeleteMapping("/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
