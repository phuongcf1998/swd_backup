/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.CompanyPayroll;
import com.example.services.CompanyPayrollService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/companyPayrolls")

public class CompanyPayrollController {

    @Autowired
    private CompanyPayrollService service;
    //get all company payroll
    @GetMapping("/readAll")
    Iterable<CompanyPayroll> readAll() {
        //out of scope
        return service.findAll();
    }
    //get company payroll by id

    @GetMapping("/read/{id}")
    CompanyPayroll read(@PathVariable String id) {
        //out of scope
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("/create")
    //create  company payroll
    CompanyPayroll create(@RequestBody CompanyPayroll newCompanyPayroll) {
        //out of scope
        return service.save(newCompanyPayroll);
    }

    @PutMapping("/update/{id}")
    //update company payroll
    CompanyPayroll update(@RequestBody CompanyPayroll editedCompanyPayroll, @PathVariable String id) {
        return service.findById(id)
                .map(CompanyPayroll -> {
                    //out of scope
                    
                    return service.save(CompanyPayroll);
                })
                .orElseGet(() -> {
                    editedCompanyPayroll.setPeriodid(id);
                    return service.save(editedCompanyPayroll);
                });
    }
    //remove company payroll
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
