/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.Company;
import com.example.services.CompanyService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/companies")

public class CompanyController {

    @Autowired
    private CompanyService service;
    //get all Companies
    @GetMapping("")
    Iterable<Company> readAll() {
        //out of scope
        return service.findAll();
    }
    
    //get Company by id

    @GetMapping("/{id}")
    Company read(@PathVariable String id) {
        //out of scope
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("")
    //create a Company
    Company create(@RequestBody Company newCompany) {
        //out of scope
        return service.save(newCompany);
    }

    @PutMapping("/{id}")
    //update Company 
    Company update(@RequestBody Company editedCompany, @PathVariable String id) {
        //out of scope
        return service.findById(id)
                .map(Company -> {
                    
                    
                    return service.save(Company);
                })
                .orElseGet(() -> {
                    editedCompany.setCompanyId(id);
                    return service.save(editedCompany);
                });
    }
    //remove a Company
    @DeleteMapping("/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
