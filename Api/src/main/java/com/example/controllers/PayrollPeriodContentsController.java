/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.PayrollPeriodContents;
import com.example.services.PayrollPeriodContent;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/PayrollPeriodContents")

public class PayrollPeriodContentsController {

    @Autowired
    private PayrollPeriodContent service;
    //get all PayrollPeriodContents
    @GetMapping("")
    Iterable<PayrollPeriodContents> readAll() {
        
        return service.findAll();
    }
    
    //get PayrollPeriodContents by id

    @GetMapping("/{id}")
    PayrollPeriodContents read(@PathVariable String id) {
        
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("")
    //create PayrollPeriodContents
    PayrollPeriodContents create(@RequestBody PayrollPeriodContents newPayrollPeriodContents) {
        //out of scope
        return service.save(newPayrollPeriodContents);
    }

    @PutMapping("/{id}")
    //update PayrollPeriodContents
    PayrollPeriodContents update(@RequestBody PayrollPeriodContents editedPayrollPeriodContents, @PathVariable String id) {
        //out of scope
        return service.findById(id)
                .map(PayrollPeriodContents -> {
                    
                    
                    return service.save(PayrollPeriodContents);
                })
                .orElseGet(() -> {
                    editedPayrollPeriodContents.setContentid(id);
                    return service.save(editedPayrollPeriodContents);
                });
    }
    //remove editedPayrollPeriodContents
    @DeleteMapping("/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
