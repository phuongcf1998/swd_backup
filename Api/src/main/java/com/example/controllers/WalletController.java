/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.Wallet;
import com.example.services.WalletService;
import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/wallets")

public class WalletController {

    @Autowired
    private WalletService service;
    //get all wallet
    @GetMapping("/readAll")
    Iterable<Wallet> readAll() {
        return service.findAll();
    }
    //get wallet by id

    @GetMapping("/read/{id}")
    Wallet read(@PathVariable String id) {
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("/create")
    //create  wallet
    Wallet create(@RequestBody Wallet newWallet) {
        //out of scope
        return service.save(newWallet);
    }

    @PutMapping("/update/{id}")
    //update wallet
    Wallet update(@RequestBody Wallet editedWallet, @PathVariable String id) {
        return service.findById(id)
                .map(Wallet -> {
                    
                    
                    return service.save(Wallet);
                })
                .orElseGet(() -> {
                    editedWallet.setWalletId(id);
                    return service.save(editedWallet);
                });
    }
    //remove wallet
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
