/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controllers;

import com.example.entities.TransactionHistory;
import com.example.services.TransactionHistoryService;
import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@RestController
@RequestMapping(value = "/transactionhistorys")

public class TransactionHistoryController {

    @Autowired
    private TransactionHistoryService service;
    //get all transaction history
    @GetMapping("/readAll")
    Iterable<TransactionHistory> readAll() {
        return service.findAll();
    }
    //get transaction history by id

    @GetMapping("/read/{id}")
    TransactionHistory read(@PathVariable String id) {
        return service.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));
    }

    @PostMapping("/create")
    //create transaction history
    TransactionHistory create(@RequestBody TransactionHistory newTransactionHistory) {
        
        return service.save(newTransactionHistory);
    }

    @PutMapping("/update/{id}")
    //update transaction history
    TransactionHistory update(@RequestBody TransactionHistory editedTransactionHistory, @PathVariable String id) {
        //out of scope
        return service.findById(id)
                .map(TransactionHistory -> {
                    
                    
                    return service.save(TransactionHistory);
                })
                .orElseGet(() -> {
                    editedTransactionHistory.setTransactionId(id);
                    return service.save(editedTransactionHistory);
                });
    }
    //remove transaction history
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable String id) {
        //out of scope
        service.deleteById(id);
    }
}
