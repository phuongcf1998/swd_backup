/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.services;

import com.example.entities.PayrollPeriodContents;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
public interface PayrollPeriodContent extends  JpaRepository<PayrollPeriodContents, String>{
    
}
