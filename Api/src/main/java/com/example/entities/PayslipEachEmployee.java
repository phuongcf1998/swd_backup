/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "payslipperEmps")
public class PayslipEachEmployee {
    //payslip id
    @Id
    @Column(name = "paychecklevel")
    private String paychecklevel;
    //pay roll id
    @Column(name = "payrollperiodcontentid")
    private String payrollperiodcontentid;
    //account id
    @Column(name = "employeeid")
    private String employeeid;
    //total point
    @Column(name = "totalpoint")
    private int totalPoint;
    //working hours point
    @Column(name = "p1")
    private int p1;
    //level point
    @Column(name = "p2")
    private int p2;
    //performence point
    @Column(name = "p3")
    private int p3;
    //up level bonus
    @Column(name = "uplevelbonus")
    private int uplevelbonus;
    //other rewards
    @Column(name = "otherrewards")
    private int otherrewards;
    public PayslipEachEmployee() {
    }

   

    public int getUplevelbonus() {
        return uplevelbonus;
    }

    public void setUplevelbonus(int uplevelbonus) {
        this.uplevelbonus = uplevelbonus;
    }

    public int getOtherrewards() {
        return otherrewards;
    }

    public void setOtherrewards(int otherrewards) {
        this.otherrewards = otherrewards;
    }

    public PayslipEachEmployee(String paychecklevel, String payrollperiodcontentid, String employeeid, int totalPoint, int p1, int p2, int p3, int uplevelbonus, int otherrewards) {
        this.paychecklevel = paychecklevel;
        this.payrollperiodcontentid = payrollperiodcontentid;
        this.employeeid = employeeid;
        this.totalPoint = totalPoint;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.uplevelbonus = uplevelbonus;
        this.otherrewards = otherrewards;
    }

    public String getPaychecklevel() {
        return paychecklevel;
    }

    public void setPaychecklevel(String paychecklevel) {
        this.paychecklevel = paychecklevel;
    }

    public String getPayrollperiodcontentid() {
        return payrollperiodcontentid;
    }

    public void setPayrollperiodcontentid(String payrollperiodcontentid) {
        this.payrollperiodcontentid = payrollperiodcontentid;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    

    

    public int getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(int totalPoint) {
        this.totalPoint = totalPoint;
    }

    public int getP1() {
        return p1;
    }

    public void setP1(int p1) {
        this.p1 = p1;
    }

    public int getP2() {
        return p2;
    }

    public void setP2(int p2) {
        this.p2 = p2;
    }

    public int getP3() {
        return p3;
    }

    public void setP3(int p3) {
        this.p3 = p3;
    }
    
}
