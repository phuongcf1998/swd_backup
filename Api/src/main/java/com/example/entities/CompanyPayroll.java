/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "payrollperiods")
public class CompanyPayroll implements Serializable {
    // payroll id
    @Id
    @Column(name = "periodid", length = 400)
    private String periodid;
    //company id
    @Column(name = "companyid", length = 400)
    private String companyid;
    //date created
    @Column(name = "date")
    private Date date;
    //time created
    @Column(name = "time")
    private Time time;
    //status
    @Column(name = "status")
    private boolean status;

    public CompanyPayroll() {
    }

    public CompanyPayroll(String periodid, String companyid, Date date, Time time, boolean status) {
        this.periodid = periodid;
        this.companyid = companyid;
        this.date = date;
        this.time = time;
        this.status = status;
    }

    public String getPeriodid() {
        return periodid;
    }

    public void setPeriodid(String periodid) {
        this.periodid = periodid;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
}
