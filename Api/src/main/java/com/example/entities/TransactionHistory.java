/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "tbltransactionhistory")
public class TransactionHistory implements Serializable {
    // transaction id
    @Id
    @Column(name = "transactionid")
    private String transactionId;
    //account id make the transaction
    @Column(name = "fromaccountid")
    private String fromAccountId;
    //account id received the transaction
    @Column(name = "toaccountid")
    private String toAccountId;
    //date make transaction
    @Column(name = "date")
    private Date date;
    //time make transaction
    @Column(name = "time")
    private Time time;
    //amount 
    @Column(name = "amount")
    private int amount;
    //transaction status
    @Column(name = "status")
    private boolean status;

    public TransactionHistory() {
    }

    public TransactionHistory(String transactionId, String fromAccountId, String toAccountId, Date date, Time time, int amount, boolean status) {
        this.transactionId = transactionId;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.date = date;
        this.time = time;
        this.amount = amount;
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(String fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(String toAccountId) {
        this.toAccountId = toAccountId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
}
