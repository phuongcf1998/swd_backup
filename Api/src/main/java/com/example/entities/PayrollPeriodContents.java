/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "payrollperiodscontents")
public class PayrollPeriodContents implements Serializable{
    //content id
    @Id
    @Column(name = "contentid")
    private String contentid;
    
    //period id
    @Column(name = "periodid")
    private String periodid;
    //employee id
    @Column(name = "employeeid")
    private String employeeid;
    //total point
    @Column(name = "totalpoint")
    private int totalpoint;

    public PayrollPeriodContents(String contentid, String periodid, String employeeid, int totalpoint) {
        this.contentid = contentid;
        this.periodid = periodid;
        this.employeeid = employeeid;
        this.totalpoint = totalpoint;
    }

    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }

    public String getPeriodid() {
        return periodid;
    }

    public void setPeriodid(String periodid) {
        this.periodid = periodid;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public int getTotalpoint() {
        return totalpoint;
    }

    public void setTotalpoint(int totalpoint) {
        this.totalpoint = totalpoint;
    }
    
}
