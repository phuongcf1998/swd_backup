/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "tblwallet")
public class Wallet implements Serializable{
    //wallet id
    @Id
    @Column(name = "walletid")
    private String walletId;
    //account id
    @Column(name = "accountid")
    private String accountId;
    //wallet type
    @Column(name = "wallettype", length = 10)
    private String walletType;
    //measurement unit
    @Column(name = "measurementunit", length = 10)
    private String measurementUnit;
    //amount
    @Column(name = "amount")
    private int amount;

    public Wallet() {
    }

    public Wallet(String walletId, String accountId, String walletType, String measurementUnit, int amount) {
        this.walletId = walletId;
        this.accountId = accountId;
        this.walletType = walletType;
        this.measurementUnit = measurementUnit;
        this.amount = amount;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
}
