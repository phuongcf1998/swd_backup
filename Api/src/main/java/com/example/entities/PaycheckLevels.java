/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "PaycheckLevels")
public class PaycheckLevels implements Serializable {
    //PaycheckLevels
    @Id
    @Column(name = "paychecklevel")
    private int paychecklevel;
    //amountperhour
    @Column(name = "amountperhour")
    private int amountperhour;

    public PaycheckLevels() {
    }

    public PaycheckLevels(int paychecklevel, int amountperhour) {
        this.paychecklevel = paychecklevel;
        this.amountperhour = amountperhour;
    }

    public int getPaychecklevel() {
        return paychecklevel;
    }

    public void setPaychecklevel(int paychecklevel) {
        this.paychecklevel = paychecklevel;
    }

    public int getAmountperhour() {
        return amountperhour;
    }

    public void setAmountperhour(int amountperhour) {
        this.amountperhour = amountperhour;
    }
    
}
