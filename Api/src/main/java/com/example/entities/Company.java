/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "company")
public class Company implements Serializable{
    //company id
    @Id
    @Column(name = "companyid", length = 400)
    private String companyId;
    //company name
    @Column(name = "companyname", length = 50)
    private String companyName;
    //director id
    @Column(name = "directoryid", length = 400)
    private String directoryId;
    //joined date
    @Column(name = "joineddate")
    private Date joinedDate;

    public Company(String companyId, String companyName, String directoryId, Date joinedDate) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.directoryId = directoryId;
        this.joinedDate = joinedDate;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(String directoryId) {
        this.directoryId = directoryId;
    }

    public Date getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(Date joinedDate) {
        this.joinedDate = joinedDate;
    }
    
}
