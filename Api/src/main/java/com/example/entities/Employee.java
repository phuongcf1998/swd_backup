/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;


import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

/**
 *
 * @author PhuNNSE62953
 * @Date created 27/11/2019
 * @Date update 27/11/2019
 * @version 0.1
 */
@Entity
@Table(name = "tblemployee")
public class Employee implements Serializable {

    
    //employee id
    @Id
    @Column(name = "employeeid", length = 400)
    private String id;
    //company id
    @Column(name = "companyid", length = 400)
    private String companyid;
    //first name
    @Column(name = "firstname")
    private String firstname;
    //last name
    @Column(name = "lastname")
    private String lastname;
    //gender
    @Column(name = "gender")
    private int gender;
    //date of birth
    @Column(name = "dob")
    private Date dob;
    //phone number
    @Column(name = "phonenumber")
    private int phoneNumber;
    //email 
    @Column(name = "email", length = 50)
    private String email;
    //address
    @Column(name = "address")
    private String address;
    //country
    @Column(name = "country")
    private String country;
    //username
    @Column(name = "username", length = 50)
    private String username;
    //password
    @Column(name = "password", length = 50)
    private String password;
    //token key
    @Column(name = "tokenkey")
    private String tokenKey;
    //level
    @Column(name = "level")
    private int level;
    public Employee() {
    }

    public Employee(String id, String companyid, String firstname, String lastname, int gender, Date dob, int phoneNumber, String email, String address, String country, String username, String password, String tokenKey, int level) {
        this.id = id;
        this.companyid = companyid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.dob = dob;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.country = country;
        this.username = username;
        this.password = password;
        this.tokenKey = tokenKey;
        this.level = level;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    

    

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
}
